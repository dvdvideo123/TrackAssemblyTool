:: Hard reset the HEAD: git push origin +<NEW_HEAD_COMMIT_HASH>:master
:: 6af74344a888bdf9fbb35d887c3f77691820a50e
:: git push origin +6af74344a888bdf9fbb35d887c3f77691820a50e:master
:: cd /e
:: cd Documents/Lua-Projs/SVN/TrackAssemblyTool_GIT_master

@echo off

set RevPath=%~dp0
set BinPath=F:\Games\Steam\steamapps\common\GarrysMod\bin
set Name=TrackAssembly
set ID=287012681
set Dirs=(lua)

title Addon %Name% updater/publisher

echo Press Crtl+C to terminate !
echo Press a key if you do not want to wait !
echo Rinning in %RevPath%.
echo Npp Find --\h{1,}\n-- replace --\n-- in dos format before commit !
echo.

timeout 3
rd /S /Q %RevPath%Workshop
md %RevPath%Workshop\%Name%
for %%i in %Dirs% do (
  echo Extracting %%i
  timeout 3
  xcopy %RevPath%%%i %RevPath%Workshop\%Name%\%%i /EXCLUDE:%RevPath%data\workshop\key.txt /E /C /I /F /R /Y
)
copy %RevPath%data\workshop\addon.json %RevPath%Workshop\%Name%\addon.json
call %BinPath%\gmad.exe create -folder "%RevPath%Workshop\%Name%" -out "%RevPath%Workshop\%Name%.gma"

timeout 15

IF DEFINED ID (
  call %BinPath%\gmpublish.exe update -addon "%RevPath%Workshop\%Name%.gma" -id "%ID%" -icon "%RevPath%data\pictures\icon.jpg" -changes "Generated by Batch!"
) ELSE (
  call %BinPath%\gmpublish.exe create -addon "%RevPath%Workshop\%Name%.gma" -icon "%RevPath%data\pictures\icon.jpg"
)

echo.
echo %Name% Published !
echo.
timeout 500
exit
